import requests


class OpenFoodFacts:

    def isVegan(barcode):
        requete = requests.get(
            "https://world.openfoodfacts.org/api/v0/product/"+str(barcode)+".json")
        requete = requete.json
        produit = requete["product"]
        if "ingredients" in produit.keys():
            ingredients = produit["ingredients"]
            isvegan = True
            for dico in ingredients:
                if "vegan" in dico.keys():
                    isvegan = ingredients["vegan"] and isvegan
            if isvegan:
                return "Oui"
        else:
            return "Ne sait pas"
