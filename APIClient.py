from typing import Optional
from fastapi import FastAPI

import OpenFoodFacts

app = FastAPI()


@app.get("/")
def read_root():
    return {"Salut": "Monde"}


@app.get("/vegan/{barcode}")
def read_item(barcode: int, q: Optional[str] = None):
    return OpenFoodFacts.isVegan(barcode)
